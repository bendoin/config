#version=RHEL7


# Use CDROM installation media
cdrom
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
#Keyboard layouts

# System language
lang en_US.UTF-8


# Root password
rootpw 12345678
#System timzone
timezone Europe/Stockholm --isUtc --nontp
user --groups=wheel --name=ladmin --password=12345678

# Firewall
firewall --enabled --ssh

# SELinux
selinux --disabled
