# File: /mnt/public/Support/Platforms/Fedora/fc27c-ks.cfg
# Locations:
#    /mnt/public/Support/Platforms/Fedora/fc27c-ks.cfg
# Author: bgstack15
# Startdate: 2017-08-10
# Title: Kickstart for Fedora 27 cinnamon for ipa.smith122.com
# Purpose: To provide an easy installation for VMs and other systems in the Mersey network
# History:
#    2017-06 I learned how to use kickstart files for the RHCSA EX-200 exam
#    2017-08-08 Added notifyemail to --extra-args
#    2017-11-01 major revision to use local mirror
#    2017-11-04 converted for building directly into an iso file
#    2017-11-18 fedora 27
# Usage with virt-install:
#    vm=fc27c-01a ; time sudo virt-install -n "${vm}" --memory 2048 --vcpus=1 --os-variant=fedora25 --accelerate -v --disk path=/var/lib/libvirt/images/"${vm}".qcow2,size=20 -l /mnt/public/Support/SetupsBig/Linux/Fedora-Workstation-netinst-x86_64-27-1.6.iso --initrd-inject=/mnt/public/Support/Platforms/Fedora/fc27c-ks.cfg --extra-args "ks=file:/fc27c-ks.cfg SERVERNAME=${vm} NOTIFYEMAIL=bgstack15@gmail.com" --debug --network type=direct,source=eno1 --noautoconsole
#    vm=fc27c-01a; sudo virsh destroy "${vm}"; sudo virsh undefine --remove-all-storage "${vm}";
# Reference:
#    https://sysadmin.compxtreme.ro/automatically-set-the-hostname-during-kickstart-installation/
#    /mnt/public/Support/Platforms/CentOS7/install-vm.txt

#platform=x86, AMD64, or Intel EM64T
#version=DEVEL
# Install OS instead of upgrade
install
# Keyboard layouts
keyboard --vckeymap=us --xlayouts=''
# Root password
rootpw --plaintext f0rg3tkickstart&
# my user
user --groups=wheel --name=bgstack15-local --password=$6$.gh9u7vg2HDJPPX/$g3X1l.q75fs7i0UKUt6h88bDIo1YSGGj/1DGeUzzbMTb0pBh4of6iNYWyxws/937qUiPgETqOsYFI5XNrkaUe. --iscrypted --gecos="bgstack15-local"

# System language
lang en_US.UTF-8
# Firewall configuration
firewall --enabled --ssh
# Reboot after installation
reboot
# Network information
#attempting to put it in the included ks file that accepts hostname from the virsh command.
#network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate
%include /tmp/network.ks
# System timezone
timezone America/New_York --isUtc
# System authorization information
auth  --useshadow  --passalgo=sha512
# Use network installation instead of CDROM installation media
url --url="http://albion320.no-ip.biz/mirror/fedora/linux/releases/27/Everything/x86_64/os/"

# Use text mode install
text
# SELinux configuration
selinux --enforcing
# Prepare X to run at boot
xconfig --startxonboot

# Use all local repositories
repo --name=smith122rpm --baseurl=https://albion320.no-ip.biz/smith122/repo/rpm/
repo --name=fedora --baseurl=https://albion320.no-ip.biz/mirror/fedora/linux/releases/$releasever/Everything/$basearch/os/
repo --name=updates --baseurl=https://albion320.no-ip.biz/mirror/fedora/linux/updates/$releasever/$basearch/
repo --name=rpmfusion-free --baseurl=https://albion320.no-ip.biz/mirror/rpmfusion/free/fedora/releases/$releasever/Everything/$basearch/os/
repo --name=rpmfusion-free-updates --baseurl=https://albion320.no-ip.biz/mirror/rpmfusion/free/fedora/updates/$releasever/$basearch/

firstboot --disabled

# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all --initlabel
# Disk partitioning information
autopart --type=lvm

%pre
echo "network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate --hostname renameme.ipa.smith122.com" > /tmp/network.ks
for x in $( cat /proc/cmdline );
do
   case $x in
      SERVERNAME*)
         eval $x
         echo "network  --bootproto=dhcp --device=eth0 --ipv6=auto --activate --hostname ${SERVERNAME}.ipa.smith122.com" > /tmp/network.ks
         ;;
      NOTIFYEMAIL*)
         eval $x
         echo "${NOTIFYEMAIL}" > /mnt/sysroot/root/notifyemail.txt
	 ;;
   esac
done
cp -p /run/install/repo/ca-ipa.smith122.com.crt /etc/pki/ca-trust/source/anchors/ 2>/dev/null || :
wget http://albion320.no-ip.biz/smith122/certs/ca-ipa.smith122.com.crt -O /etc/pki/ca-trust/source/anchors/ca-ipa.smith122-wget.com.crt || :
update-ca-trust || :
%end

%post
(
   # Set temporary hostname
   #hostnamectl set-hostname renameme.ipa.smith122.com;

   # Get local mirror root ca certificate
   wget http://albion320.no-ip.biz/smith122/certs/ca-ipa.smith122.com.crt -O /etc/pki/ca-trust/source/anchors/ca-ipa.smith122.com.crt && update-ca-trust

   # Get local mirror repositories
   wget https://albion320.no-ip.biz/smith122/repo/rpm/smith122rpm.repo -O /etc/yum.repos.d/smith122rpm.repo;
   distro=fc27 ; sudo wget https://albion320.no-ip.biz/smith122/repo/mirror/smith122-bundle-${distro}.repo -O /etc/yum.repos.d/smith122-bundle-${distro}.repo && grep -oP "(?<=^\[).*(?=-smith122])" /etc/yum.repos.d/smith122-bundle-${distro}.repo | while read thisrepo; do sudo dnf config-manager --set-disabled "${thisrepo}"; done
   dnf -y remove dnfdragora ;
   dnf clean all ;
   dnf update -y ;

   # Remove graphical boot and add serial console
   sed -i -r -e '/^GRUB_CMDLINE_LINUX=/{s/(\s*)(rhgb|quiet)\s*/\1/g;};' -e '/^GRUB_CMDLINE_LINUX=/{s/(\s*)\"$/ console=ttyS0 console=tty1\"/;}' /etc/default/grub
   grub2-mkconfig > /boot/grub2/grub.cfg

   systemctl enable sendmail.service && systemctl start sendmail.service
   # Send IP address to myself
   thisip="$( ifconfig 2>/dev/null | awk '/Bcast|broadcast/{print $2}' | tr -cd '[^0-9\.\n]' | head -n1 )"
   {
      echo "${SERVER} has IP ${thisip}."
      echo "system finished kickstart at $( date "+%Y-%m-%d %T" )";
   } | /usr/share/bgscripts/send.sh -f "root@$( hostname --fqdn )" \
      -h -s "${SERVER} is ${thisip}" $( cat /root/notifyemail.txt 2>/dev/null )

   # Ensure boot to runlevel 5
   systemctl set-default graphical.target

   # Personal customizations
   mkdir -p /mnt/bgstack15 /mnt/public
   su bgstack15-local -c "sudo /usr/share/bgconf/bgconf.py"

) >> /root/install.log 2>&1
%end

%packages
@core
@^cinnamon-desktop-environment
#@cinnamon-desktop
#
#@basic-desktop
autossh
bc
bgconf
bgscripts
bgscripts-core
bind-utils
cifs-utils
cryptsetup
-dnfdragora
dosfstools
expect
firewalld
freeipa-client
git
-hplip
iotop
mailx
man
mlocate
net-tools
nfs-utils
ntp
numix-icon-theme-circle
p7zip
parted
policycoreutils-python
qemu-guest-agent
rpm-build
rsync
scite
screen
sendmail
spice-vdagent
strace
sysstat
tcpdump
telnet
-thunderbird
vim
vlc
wget
#
-gstreamer1-plugins-ugly*
%end