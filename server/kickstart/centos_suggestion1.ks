#version=DEVEL
install
skipx
text
lang en_GB.UTF-8
keyboard uk
install
url --url http://mirror.centos.org/centos/6/os/x86_64/
#network --onboot yes --device eth0 --bootproto dhcp --noipv6
rootpw  --iscrypted $6$RJuCzguSVNJr1WYH$D6XPhV027pG2MBeivbfjA8S6TD/JHUWcIrJxiuNuRmQ2Wm4MQgTmvIbrJKZ5h0TnLEGHMn6Dhml6Bqpvc1xjP/
firewall --service=ssh
selinux --enforcing
timezone --utc Etc/UTC
zerombr
bootloader --location=mbr --driveorder=sda,sdb,sdc,sdd --append="crashkernel=auto quiet vga=0x305" --password=$6$EOGuT84EUOW7GuUK$ijfJ7PwqPQqNNS0W7PqWAU8RHpbvTfHH/BN72lPMiWMlcIO0xgxRKL6NaEFyztlqob4cr/zYwCR5GN4KM/1gJ0
clearpart --all
autopart 
%include /tmp/network.ks

repo --name="CentOS"  --baseurl="http://mirror.centos.org/centos/6/os/x86_64/" --cost=100
repo --name="EPEL 6 - x86_64" --baseurl="http://dl.fedoraproject.org/pub/epel/6Server/x86_64/" --cost=80

%packages
@core
@server-policy
vim
tuned
openssh-clients
ipa-client
zsh
epel-release
-libX11
-mysql-server
-redhat-logos
-aic94xx-firmware
-atmel-firmware
-b43-openfwwf
-bfa-firmware
-ipw2100-firmware
-ipw2200-firmware
-ivtv-firmware
-iwl100-firmware
-iwl1000-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-libertas-usb8388-firmware
-netxen-firmware
-ql2100-firmware
-ql2200-firmware
-ql23xx-firmware
-ql2400-firmware
-ql2500-firmware
-rt61pci-firmware
-rt73usb-firmware
-xorg-x11-drv-ati-firmware
-zd1211-firmware
%end

%pre
#!/bin/sh
for x in `cat /proc/cmdline`; do
        case $x in ip*)
	        eval $x
        echo -n "network --device eth0 --bootproto static --ip ${ip} --netmask 255.255.255.0 --gateway 192.168.15.1 --nameserver=192.168.15.1 " > /tmp/network.ks ;;
        SERVERNAME*)
	        eval $x
		echo "--hostname ${SERVERNAME} noipv6" >> /tmp/network.ks ;;
	        esac;
	done
%end

%post
#authconfig --update --kickstart --enablecache --enablemkhomedir --enableldap --enableldapauth --ldapserver=ldap://brockman.firebox.com:389 --ldapbasedn="dc=firebox,dc=com" 

echo "%systems ALL=(ALL) ALL" >> /etc/sudoers

echo "AllowGroups systems" >> /etc/ssh/sshd_config

echo "exclude = *.i?86" >> /etc/yum.conf 

echo "# Require the root pw when booting into single user mode" >> /etc/inittab
echo "~~:S:wait:/sbin/sulogin" >> /etc/inittab
echo "Don't allow any nut to kill the server"
perl -npe 's/ca::ctrlaltdel:\/sbin\/shutdown/#ca::ctrlaltdel:\/sbin\/shutdown/' -i /etc/inittab

echo "Disabling USB Mass Storage"
echo "blacklist usb-storage" > /etc/modprobe.d/blacklist-usbstorage.conf

echo "tty1" > /etc/securetty
chmod 700 /root

perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/bashrc
perl -npe 's/umask\s+0\d2/umask 077/g' -i /etc/csh.cshrc

touch /var/log/tallylog
cat << 'EOF' > /etc/pam.d/system-auth
#%PAM-1.0
# This file is auto-generated.
# User changes will be destroyed the next time authconfig is run.
auth        required      pam_env.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 500 quiet
auth        required      pam_deny.so
auth        required      pam_tally2.so deny=3 onerr=fail unlock_time=60

account     required      pam_unix.so
account     sufficient    pam_succeed_if.so uid < 500 quiet
account     required      pam_permit.so
account     required      pam_tally2.so per_user

password    requisite     pam_cracklib.so try_first_pass retry=3 minlen=9 lcredit=-2 ucredit=-2 dcredit=-2 ocredit=-2
password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok remember=10
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
EOF

tuned-adm profile virtual-guest

echo "Idle users will be removed after 5 minutes"
echo "readonly TMOUT=300" >> /etc/profile.d/os-security.sh
echo "readonly HISTFILE" >> /etc/profile.d/os-security.sh
chmod +x /etc/profile.d/os-security.sh

echo "Locking down Cron"
touch /etc/cron.allow
chmod 600 /etc/cron.allow
awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/cron.deny
echo "Locking down AT"
touch /etc/at.allow
chmod 600 /etc/at.allow
awk -F: '{print $1}' /etc/passwd | grep -v root > /etc/at.deny

for i in $(find /lib/modules/`uname -r`/kernel/drivers/net/wireless -name "*.ko" -type f) ; do echo blacklist $i >> /etc/modprobe.d/blacklist-wireless.conf ; done

cat << 'EOF' >> /etc/sysctl.conf
# Added by kickstart, because, reasons.
net.ipv4.ip_forward = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.tcp_max_syn_backlog = 1280
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.all.log_martians = 1
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.tcp_syncookies = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.conf.default.rp_filter = 1
net.ipv4.tcp_timestamps = 0
EOF

zcat /lib/kbd/keymaps/i386/qwerty/uk.map.gz | sed 's/Caps_Lock/Control/'> \
 /lib/kbd/keymaps/i386/qwerty/uk.map && \
  rm -f /lib/kbd/keymaps/i386/qwerty/uk.map.gz ; 
gzip /lib/kbd/keymaps/i386/qwerty/uk.map ;
chmod 644 /lib/kbd/keymaps/i386/qwerty/uk.map.gz && \
 chcon -u system_u /lib/kbd/keymaps/i386/qwerty/uk.map.gz
%end
poweroff
