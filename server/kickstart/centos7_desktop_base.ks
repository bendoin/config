version=DEVEL
# System authorization information
auth --enableshadow --passalgo=sha512
# Use CDROM installation media
cdrom
# Use graphical install
graphical
# Run the Setup Agent on first boot
firstboot --enable
ignoredisk --only-use=sda
# Keyboard layouts
keyboard --vckeymap=se --xlayouts='se','us'
# System language
lang en_CA.UTF-8

# Network information
network  --bootproto=dhcp --device=enp0s3 --ethtool="wol d" --ipv6=auto --activate
network  --hostname=desktopy.localdomain

# Root password
rootpw --iscrypted $6$pPSL/775y2DhDNfh$nCPX.vogIpTzGXD.vG0Pg3f0DZo4q.NAC04/hXNz7a6mxfUYrCEe/PJERdYAERmGOq85kYBMkdS7we5dTsTmJ/
# System services
services --enabled="chronyd"
# System timezone
timezone America/Edmonton --isUtc
user --groups=wheel --name=localadmin --password=$6$lhsFG.MNo1WdP3bO$rkKyFwUvDyCPhpkOfdrg9H1Ph8Ob6MYNoG5mBneKnxTfDJumLyUEG22l62bPRUpLdVgZ/W4DPazM7RAkAA2lr/ --iscrypted --uid=5001 --gecos="localadmin" --gid=5001
# X Window System configuration information
xconfig  --startxonboot
# System bootloader configuration
bootloader --location=mbr --boot-drive=sda
autopart --type=lvm
# Partition clearing information
# Partition clearing information
clearpart --none --initlabel

%packages
@^graphical-server-environment
@base
@core
@desktop-debugging
@dial-up
@fonts
@gnome-desktop
@guest-agents
@guest-desktop-agents
@hardware-monitoring
@input-methods
@internet-browser
@multimedia
@print-client
@x11
chrony

%end

%addon com_redhat_kdump --disable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end

