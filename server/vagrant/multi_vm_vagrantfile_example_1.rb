# -*- mode: ruby -*-
# vi: set ft=ruby : 
# Source: https://gist.github.com/dlutzy/2469037

boxes = [
  { :name => :web,      :role => 'web_dev',      :ip => '192.168.33.1', :ssh_port => 2201, :http_fwd => 9980, :cpus =>4, :shares => true },
  { :name => :data,     :role => 'data_dev',     :ip => '192.168.33.2', :ssh_port => 2202, :mysql_fwd => 9936, :cpus =>4 },
  { :name => :railsapp, :role => 'railsapp_dev', :ip => '192.168.33.3', :ssh_port => 2203, :http_fwd => 9990, :cpus =>1}
]

Vagrant::Config.run do |config|

   chef_default = proc do |chef|
    chef.cookbooks_path = "cookbooks"
    chef.roles_path     = "roles"
  end

  boxes.each do |opts|

    config.vm.define opts[:name] do |config|
    config.vm.box        = "ubuntu-11.10-server-amd64-ruby1.9.3"
    config.vm.box_url    = "http://hostname/boxes/ubuntu-11.10-server-amd64-ruby1.9.3.box"
    config.vm.customize  ["modifyvm", :id, "--memory", 1024]
    #todo make this an option

    config.vm.forward_port  80, opts[:http_fwd] if opts[:http_fwd]
    config.vm.forward_port  3306, opts[:mysql_fwd] if opts[:mysql_fwd]
    config.vm.network       :hostonly, opts[:ip]
    config.vm.host_name =   "%s.vagrant" % opts[:name].to_s

    config.vm.share_folder  "stuff", "/usr/local/stuff", "~/Projects/stuff", :nfs => true if opts[:shares]
    # use nfs rather than VirtualBox shared files.  It's heaps faster.

    config.vm.forward_port  22, opts[:ssh_port], :auto => true

    config.vm.customize ["modifyvm", :id, "--cpus", opts[:cpus] ] if opts[:cpus] 
    # cpus defaults to 1

     config.vm.provision :chef_solo do |chef|
       chef_default.call(chef)
       chef.add_role opts[:role].to_s
     end
    end

  end
end
