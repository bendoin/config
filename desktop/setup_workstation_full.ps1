#Requires -runasadministrator
#Requires -version 5

<#

## Sources:
   https://gist.github.com/jessfraz/7c319b046daa101a4aaef937a20ff41f
#>

###############################################################################
## Variables ##################################################################
###############################################################################

$choco_executable = "C:\ProgramData\chocolatey\bin\choco.exe"

###############################################################################
## Admin level stuff
###############################################################################
 
function install_chocolatey () {

    if (! (test-path $choco_executable)) {

        write-output "Installing Chocolatey..."
        Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
        write-output "Installation done..."
    } else {

        write-output "Choco detected, skipping installation... Don't forget to update once in a while..."
    }
}

function install_wsl () {

    # After installation your Linux distribution will be located at: %localappdata%\lxss\
    # https://msdn.microsoft.com/en-us/commandline/wsl/install-win10
    # TODO(Mattias): Install an actual shell as well from the store somehow

    if ((Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux).State -eq "Disabled") {

        write-output "Installing Windows Subsystem for Linux..."

        Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

        write-output "Installation done..."

        $restart_question = Read-Host 'A restart is required to proceed with installation. Restart now? (y/n)'

       if ($restart_question.tolower() -eq "y") {

           write-output "Restarting in 3...2...1..."
           shutdown /f /r /t 3
       } else {

           write-output "No restart for you. Please restart at your convenience..."
       }

    } else {

        write-output "Windows Subsystem for Linux detected, skipping installation..."
    }
}

function install_ubuntu_wsl () {

    <# 
      TODO: Test me.
      Ubuntu 18.04 cannot be installed here.

      Either install 16.04 intead and do dist upgrade
      or install manaully from Microsoft Store.
      
      This will install the Ubuntu as provided in Microsoft Store.
      But will not use Microsoft store for it. Yay!

      https://docs.microsoft.com/en-us/windows/wsl/install-manual
      https://docs.microsoft.com/en-us/windows/wsl/install-on-server
      https://docs.microsoft.com/en-us/windows/wsl/initialize-distr
    #>

    if (!(Get-AppxPackage -name CanonicalGroupLimited.Ubuntu18.04onWindows)) {

      write-output "Ubuntu 18.04 not detected, please install it manually from here: https://www.microsoft.com/store/productId/9N9TNGVNDL3Q"
    } elseif ((Get-AppxPackage -name CanonicalGroupLimited.Ubuntu16.04onWindows)) {

      write-output "Ubuntu 16.04 detected..."
      # write-output "Installing Ubuntu 16.04..."

      # $ubuntu_installer_tmp_dir = "$env:windir\temp"
      # $ubuntu_installer_url     = "https://aka.ms/wsl-ubuntu-1604"

      # start-process -wait -filepath curl.exe -argumentlist "-L -o $ubuntu_installer_tmp_dir\ubuntu.appx $ubuntu_installer_url"

      # rename-item $ubuntu_installer_tmp_dir\ubuntu.appx $ubuntu_installer_tmp_dir\ubuntu.zip

      # Expand-Archive -path $ubuntu_installer_tmp_dir\ubuntu.zip -destinationpath $ubuntu_installer_tmp_dir

      # start-process -wait -filepath $ubuntu_installer_tmp_dir\ubuntu\ubuntu.exe

      # write-output "Ubuntu installation done... you must now initialize it to be able to use it..."

      # remove-item -path $ubuntu_installer_tmp_dir\ubuntu.zip, $ubuntu_installer_tmp_dir\u*buntu\ -Force

      # write-output "Installation done..."
    } else {

        write-output "Ubuntu detected, skipping installation..."
    }
}

function install_openssh () {

    # Source: https://poweruser.blog/enabling-the-hidden-openssh-server-in-windows-10-fall-creators-update-1709-and-why-its-great-51c9d06db8df
    try {
        Write-Output "Installing openssh client..."
        dism /online /Add-Capability /CapabilityName:OpenSSH.Client~~~~0.0.1.0
    }

    catch {
        Write-Output "Failed to install openssh client..."
    }

    # TODO: check if installed in c:\windows\system32\OpenSSH
    try {
        Write-Output "Installing openssh server..."
        dism /online /Add-Capability /CapabilityName:OpenSSH.Server~~~~0.0.1.0
    }

    catch {
        Write-Output "Failed to install openssh server..."
    }

    # Create sshd server keys

    # if (!(Test-Path c:\windows\system32\openssh\ssh_host_ed25519_key)) {
        Set-Location C:\Windows\system32\OpenSSH
        ssh-keygen -A
    # }

    try {
        Write-Output "Enabling sshd service..."
        Start-Service sshd
    }

    catch {
        write-output " Failed to enable ssh service..."
    }
}

function disable_ipv6_tunnel_protocols() {

    # Removes/disabled IPv6 tunnel protocols enabled by default

    netsh interface isatap set state disabled
}

function update_shit() {

  Write-Output "Updating all Choco packages..."

  cup all -y

  Write-Output "Updating done..."
}

function install_choco_packages() {

    $base_packages = @(
        "VisualStudioCode",
        "googlechrome",
        "firefox",
        "putty",
        "curl",
        "vlc",
        "7zip",
        "cmder",
        "Inconsolata", # nice monospace font
        "firacode"     # https://github.com/tonsky/FiraCode/
    )

    $game_packages = @(
        "steam",
        "uplay",
        "discord",
        "geforce-experience",
        "logitechgaming"
        "teamspeak"
    )

    $development_packages = @(
        "git",
        "virtualbox",
        "VirtualBox.ExtensionPack",
        "vagrant",
        "packer",
        "golang",
        "python3",
        #"pyqt5", # Don't need it just yet
        "PyCharm-community",
        "postman",
        "pgadmin4", # Postgresql admin tool
        "neovim"    # gvim instead?
    )

    $additional_packages = @(
        "sketchup",
        "hexchat",
        "slack",
        "filezilla", # (s)FTP client
        #"skype",    # Install is broken on Chocolatey
        #"spotify",  # Install is broken on Chocolatey
        "keepassxc",
        "lastpass",  # Lastpass password manager
        "figlet-go", # Figlet ported to gos. URL and what is this?
        "nmap",
        "imgburn",
        "openvpn",
        "MobaXTerm", # SSH/RDP/VNC/SFTP
        "calibre"    # [e-book management/converter](https://calibre-ebook.com)
    )

    $graphics_software = @(
        "inkscape",      # [Vector graphics software](https://inkscape.org/en/)
        "gimp",          # [Graphics editor](https://www.gimp.org/)
        "imagemagick",   # [Image manipulation software](https://www.imagemagick.org)
        #"scribus",      # [Deskop Publishing tool](https://www.scribus.net/)
        #"swatchbooker", # [Not available yet. Color palette software](http://www.selapa.net/swatchbooker/)
        "darktable"      # [Photography workflow application](https://www.darktable.org/)
    )

    write-Output "Installing all teh Choco packages..."
    choco install $base_packages -y
    choco install $game_packages -y
    choco install $development_packages -y
    choco install $additional_packages -y
    choco install $graphics_software -y

    # Powershell refresh environment variables might be needed.
    refreshenv

    write-Output "Installation done... Don't forget to update once in a while..."
}


function windows_settings() {
    #Not working, need import of features first?

    Disable-BingSearch
    Disable-GameBarTips
    Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowProtectedOSFiles -EnableShowFileExtensions
    Set-TaskbarOptions -Size Small -Dock Bottom -Combine Full -Lock
    Set-TaskbarOptions -Size Small -Dock Bottom -Combine Full -AlwaysShowIconsOn
}

function remove_windows_store_applications() {

    Write-Output "Trying to remove some Microsoft Store applicaions..."
    $windows_store_applications = @(
        "Microsoft.MicrosoftStickyNotes",
        "Microsoft.SkypeApp",
        "Microsoft.BingWeather",
        "Microsoft.BingFinance",
        "Microsoft.BingNews",
        "Microsoft.BingSports",
        "Microsoft.BingWeather",
        "Microsoft.Office.OneNote",
        "Microsoft.Office.Sway",
        "Microsoft.XboxApp",
        "Microsoft.XboxIdentityProvider",
        "SpotifyAB.SpotifyMusic"
    )

    foreach ($windows_store_application in $windows_store_applications) {

        Get-AppxPackage "$windows_store_application" | Remove-AppxPackage
    }

    Write-Output "Removal done..."
}

function disable_windows_functions() {

    # Powershell beep
    Set-Service beep -StartupType disabled
    Stop-Service beep -Force -Erroraction SilentlyContinue
}

function install_hexchat_theme () {

  if (test-path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\HexChat") {

    write-output "Hexchat detected... installing theme(s)..."

    if (! (test-path $env:APPDATA\HexChat\Monokai.hct)) {

        if (get-process -name hexchat -ErrorAction SilentlyContinue) {

            write-output "Please close hexchat before installing a theme, aborting theme install..."
        } else {

            try {

                write-output "Downloading hexchat monokai theme..."
                Invoke-WebRequest "https://dl.hexchat.net/themes/Monokai.hct" -OutFile "$env:APPDATA\HexChat\Monokai.hct"
                Get-ChildItem $env:APPDATA\HexChat\Monokai.hct | % {& "C:\Program Files\7-Zip\7z.exe" "x" $_.fullname "-o$env:APPDATA\HexChat\"}
                write-output "Download done, unpacking done..."
            }

            Catch {
                write-output "Failed to download hexchat monokai theme..."
            }
        }
    } else {

        # pass
        write-output "$env:APPDATA\HexChat\Monokai.hct detected, nothing to install..."
    }

    Write-Output "Installation done..."
  }
}

function remove_shortcuts_on_desktop () {

    Write-Output "Removing desktop shortcut links for user and Public..."
    Remove-Item -Path $ENV:HOMEPATH\Desktop\*.lnk -Force
    Remove-Item -Path $ENV:PUBLIC\Desktop\*.lnk -Force

    Write-Output "Removal done..."
}

###############################################################################
## Non admin level stuff
###############################################################################

function install_visualstudiocode_extentions() {

  if (test-path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio Code") {

    # https://code.visualstudio.com/docs/editor/extension-gallery

    write-output "Visual Studio Code detected... installing extension(s)..."

    $visualstudiocode_extentions = @(
        "DavidAnson.vscode-markdownlint",
        "geeklearningio.graphviz-markdown-preview",
        "stephanvs.dot",
        "vscoss.vscode-ansible",
        "redhat.vscode-yaml",
        "ms-vscode.powershell",
        "ms-python.python",
        "ms-vscode.go"
    )

    foreach ($visualstudiocode_extention in $visualstudiocode_extentions) {

        code --install-extension $visualstudiocode_extention
    }

    write-output "Installation done... Please reload Visual Studio Code..."
  }
}

function mount_network_shares() {

    # TODO Do not run as admin, but as the user logged in
    invoke-command { net use u: \\fileserver\$env:username /persistent:yes }
}

function set_environment_variables () {

    # Get-ChildItem Env:
    # get-childitem env:GIT_SSH| sort-object -property name
    # $env:GIT_SSH = "C:\ProgramData\chocolatey\bin\plink.exe" # Only works for single session

    # Add SSH host key beforehand
    # ssh-ed25519 256 2e:65:6a:c8:cf:bf:b2:8b:9a:bd:6d:9f:11:5c:12:16

    # Git SSH key read from putty
    [Environment]::SetEnvironmentVariable("GIT_SSH", "C:\ProgramData\chocolatey\bin\plink.exe", "User")

    # Might work, restart, or at least restart of console might be needed
    refreshenv
}

function git_settings () {

    # git config --list
    git config --global user.name "$env:username"
    # TODO(Mattias) get e-mail address from somewhere?
    git config --global user.email "$env:username`@lamerschool.com"
    git config --global core.editor code
}

###############################################################################
## Execution
###############################################################################

write-output "Time to install me some things..."

# Base installations
install_chocolatey
install_wsl
install_ubuntu_wsl
disable_ipv6_tunnel_protocols

# Application installation
# install_openssh
install_choco_packages
install_visualstudiocode_extentions

# Application additions
install_hexchat_theme

# Clean up shit
remove_windows_store_applications
disable_windows_functions
remove_shortcuts_on_desktop

$update_choco_packages = 'Installation done, do you want to update all Choco packages? (y/N)'

if ($update_choco_packages.ToLower() -eq "y") {

  update_shit
}

write-output "All done..."
