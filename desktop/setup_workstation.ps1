#Requires -runasadministrator
#Requires -version 5

<#
TODO: Write synopsis.
#>

###############################################################################
## Variables ##################################################################
###############################################################################

$choco_executable = "C:\ProgramData\chocolatey\bin\choco.exe"

###############################################################################
## Functions###################################################################
###############################################################################
 
function install_chocolatey () {

    if (! (test-path $choco_executable)) {

        write-output "Installing Chocolatey..."
        Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
        write-output "Installation done..."
    } else {

        write-output "Choco detected, skipping installation... Don't forget to update once in a while..."
    }
}

function install_wsl () {

    # After installation your Linux distribution will be located at: %localappdata%\lxss\
    # https://msdn.microsoft.com/en-us/commandline/wsl/install-win10
    # TODO(Mattias): Install an actual shell as well from the store somehow

    if ((Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux).State -eq "Disabled") {

        write-output "Installing Windows Subsystem for Linux..."

        Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

        write-output "Installation done..."

        $restart_question = Read-Host 'A restart is required to proceed with installation. Restart now? (y/n)'

       if ($restart_question.tolower() -eq "y") {

           write-output "Restarting in 3...2...1..."
           shutdown /f /r /t 3
       } else {

           write-output "No restart for you. Please restart at your convenience..."
       }

    } else {

        write-output "Windows Subsystem for Linux detected, skipping installation..."
    }
}

function install_ubuntu_wsl () {

    <# 
      TODO: Test me.
      Ubuntu 18.04 cannot be installed here.

      Either install 16.04 intead and do dist upgrade
      or install manaully from Microsoft Store.
      
      This will install the Ubuntu as provided in Microsoft Store.
      But will not use Microsoft store for it. Yay!

      https://docs.microsoft.com/en-us/windows/wsl/install-manual
      https://docs.microsoft.com/en-us/windows/wsl/install-on-server
      https://docs.microsoft.com/en-us/windows/wsl/initialize-distr
    #>

    if (!(Get-AppxPackage -name CanonicalGroupLimited.Ubuntu18.04onWindows)) {

      write-output "Ubuntu 18.04 not detected, please install it manually from here: https://www.microsoft.com/store/productId/9N9TNGVNDL3Q"
    } elseif ((Get-AppxPackage -name CanonicalGroupLimited.Ubuntu16.04onWindows)) {

      write-output "Ubuntu 16.04 detected..."
      # write-output "Installing Ubuntu 16.04..."

      # $ubuntu_installer_tmp_dir = "$env:windir\temp"
      # $ubuntu_installer_url     = "https://aka.ms/wsl-ubuntu-1604"

      # start-process -wait -filepath curl.exe -argumentlist "-L -o $ubuntu_installer_tmp_dir\ubuntu.appx $ubuntu_installer_url"

      # rename-item $ubuntu_installer_tmp_dir\ubuntu.appx $ubuntu_installer_tmp_dir\ubuntu.zip

      # Expand-Archive -path $ubuntu_installer_tmp_dir\ubuntu.zip -destinationpath $ubuntu_installer_tmp_dir

      # start-process -wait -filepath $ubuntu_installer_tmp_dir\ubuntu\ubuntu.exe

      # write-output "Ubuntu installation done... you must now initialize it to be able to use it..."

      # remove-item -path $ubuntu_installer_tmp_dir\ubuntu.zip, $ubuntu_installer_tmp_dir\u*buntu\ -Force

      # write-output "Installation done..."
    } else {

        write-output "Ubuntu detected, skipping installation..."
    }
}

function install_openssh () {

    # Sources: 
    # https://github.com/PowerShell/Win32-OpenSSH/wiki/Install-Win32-OpenSSH
    # https://poweruser.blog/enabling-the-hidden-openssh-server-in-windows-10-fall-creators-update-1709-and-why-its-great-51c9d06db8df

    try {
        Write-Output "Installing openssh client..."
        dism /online /Add-Capability /CapabilityName:OpenSSH.Client~~~~0.0.1.0
    }

    catch {
        Write-Output "Failed to install openssh client..."
    }

    # TODO: check if installed in c:\windows\system32\OpenSSH
    try {
        Write-Output "Installing openssh server..."
        dism /online /Add-Capability /CapabilityName:OpenSSH.Server~~~~0.0.1.0
    }

    catch {
        Write-Output "Failed to install openssh server..."
    }

    # Create sshd server keys

    # if (!(Test-Path c:\windows\system32\openssh\ssh_host_ed25519_key)) {
        Set-Location C:\Windows\system32\OpenSSH
        ssh-keygen -A
    # }

    try {
        Write-Output "Enabling sshd service..."
        Start-Service sshd
    }

    catch {
        write-output " Failed to enable ssh service..."
    }
}

###############################################################################
## Execution ##################################################################
###############################################################################

write-output "Bootstrapping node..."

install_chocolatey
install_wsl
install_ubuntu_wsl

write-output "All done..."
